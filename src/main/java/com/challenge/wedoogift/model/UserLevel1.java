package com.challenge.wedoogift.model;

import com.google.gson.annotations.Expose;

public class UserLevel1 extends User {

	@Expose private int balance;

	public UserLevel1(int balance) {
		super();
		this.balance = balance;
	}
	
	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

}
