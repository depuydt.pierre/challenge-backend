package com.challenge.wedoogift.model;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

import com.challenge.wedoogift.util.CompanySerializer;
import com.challenge.wedoogift.util.UserSerializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

public class Distribution {
	private static final AtomicInteger count = new AtomicInteger(0); 
	@Expose private long id;
	
	@Expose private int amount;
	
	@Expose private LocalDate startDate;
	
	@Expose private LocalDate endDate;

	
	@JsonAdapter(CompanySerializer.class)
	@SerializedName("company_id")
	@Expose private Company company;
	
	@JsonAdapter(UserSerializer.class)
	@SerializedName("user_id")
	@Expose private User user;

	public Distribution(Integer amount, LocalDate startDate) {
		this.id = count.incrementAndGet();
		this.amount = amount;
		this.startDate = startDate;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
