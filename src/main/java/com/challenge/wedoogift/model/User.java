package com.challenge.wedoogift.model;

import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.annotations.Expose;

public class User {
	private static final AtomicInteger count = new AtomicInteger(0); 
	@Expose private long id;


	public User() {
		this.id = count.incrementAndGet();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


}
