package com.challenge.wedoogift.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class EndowmentLevel2 extends Endowment {

	@Expose(serialize = false) private List<Wallet> wallets;
	
	public List<Wallet> getWallets() {
		return wallets;
	}
	public void setWallets(List<Wallet> wallets) {
		this.wallets = wallets;
	}
}
