package com.challenge.wedoogift.model;

import java.time.LocalDate;

import com.challenge.wedoogift.util.WalletSerializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

public class DistributionLevel2 extends Distribution{

	@JsonAdapter(WalletSerializer.class)
	@SerializedName("wallet_id")
	@Expose private Wallet wallet;
	
	public DistributionLevel2(Integer amount, LocalDate startDate, Wallet wallet) {
		super(amount, startDate);
		this.wallet = wallet;
	}
	
	public DistributionLevel2(Distribution distribution, Wallet wallet) {
		super(distribution.getAmount(), distribution.getStartDate());
		this.wallet = wallet;
	}
	
	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}
	public Wallet getWallet() {
		return wallet;
	}

}
