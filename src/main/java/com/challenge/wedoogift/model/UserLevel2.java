package com.challenge.wedoogift.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class UserLevel2 extends User{
	
	@Expose private List<Balance> balance;
	
	public UserLevel2(List<Balance> balance) {
		super();
		this.balance = balance;
	}
	
	public UserLevel2(User user,List<Balance> balance) {
		super();
		this.balance = balance;
	}

	public List<Balance> getBalance() {
		return balance;
	}

	public void setBalance(List<Balance> balance) {
		this.balance = balance;
	}
}
