package com.challenge.wedoogift.model;

import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.annotations.Expose;

public class Wallet {
	
	private static final AtomicInteger count = new AtomicInteger(0); 
	@Expose private long id;
	
	@Expose private String name;
	
	@Expose private WalletType type;
	
	public Wallet(long id, String name, WalletType type) {
		super();
		this.id = count.incrementAndGet();
		this.name = name;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WalletType getType() {
		return type;
	}

	public void setType(WalletType type) {
		this.type = type;
	}


	
}
