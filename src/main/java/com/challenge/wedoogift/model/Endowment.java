package com.challenge.wedoogift.model;

import java.util.List;


import com.google.gson.annotations.Expose;


public class Endowment {


	@Expose private List<Company> companies;
	@Expose private List<User> users;
	@Expose private List<Distribution> distributions;

	public List<Company> getCompanies() {
		return companies;
	}
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public List<Distribution> getDistributions() {
		return distributions;
	}
	public void setDistributions(List<Distribution> distributions) {
		this.distributions = distributions;
	}

}
