package com.challenge.wedoogift.model;

import com.google.gson.annotations.Expose;

public class Balance {

	
	@Expose private int amount;
	
	@Expose private long wallet_id;

	public Balance(int amount, Wallet wallet) {
		super();
		this.amount = amount;
		this.wallet_id = wallet.getId();
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public long getWallet() {
		return wallet_id;
	}

	public void setWallet(long wallet_id) {
		this.wallet_id = wallet_id;
	}
}
