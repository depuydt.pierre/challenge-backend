package com.challenge.wedoogift.model;

import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.annotations.Expose;

public class Company {

	private static final AtomicInteger count = new AtomicInteger(0); 
	@Expose private long id;
	
	@Expose private String name;
	
	@Expose private Integer balance;

	public Company(String name, Integer balance) {
		super();
		this.id = count.incrementAndGet();
		this.name = name;
		this.balance = balance;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}
}
