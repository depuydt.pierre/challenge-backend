package com.challenge.wedoogift;

import java.time.LocalDate;

import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.Endowment;
import com.challenge.wedoogift.service.DistributionService;
import com.challenge.wedoogift.service.DistributionServiceImpl;
import com.challenge.wedoogift.service.JsonService;
import com.challenge.wedoogift.service.JsonServiceImpl;
import com.challenge.wedoogift.service.UserServiceImpl;

public class MainLevel1 {
	private final static String inputFile = "backend/Level1/data/input.json";
	private final static String outputFile = "backend/Level1/data/output.json";
	public static void main(String[] args) throws Exception {
		UserServiceImpl userService = new UserServiceImpl();
		DistributionService distributionService = new DistributionServiceImpl(userService);
		JsonService jsonService = new JsonServiceImpl();
		Endowment end = jsonService.parseJson(inputFile, Endowment.class);
		Distribution distribution1 = new Distribution(50, LocalDate.of(2021,9,16));
		Distribution distribution2 = new Distribution(100, LocalDate.of(2021,8,1));
		Distribution distribution3 = new Distribution(1000, LocalDate.of(2021,5,1));
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(0), distribution1);
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(1), distribution2);
		distributionService.distributeGiftCard(end.getCompanies().get(1), end.getUsers().get(2), distribution3);
		end.getDistributions().add(distribution1);
		end.getDistributions().add(distribution2);
		end.getDistributions().add(distribution3);
		jsonService.convertToJson(end, outputFile);
	}

}
