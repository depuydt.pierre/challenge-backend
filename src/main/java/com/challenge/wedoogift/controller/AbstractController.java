package com.challenge.wedoogift.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.wedoogift.service.DistributionService;
import com.challenge.wedoogift.service.EndowmentService;
import com.challenge.wedoogift.service.JsonService;
import com.challenge.wedoogift.service.UserService;

@RestController
public class AbstractController {

	@Autowired
	@Qualifier("distributionService1")
	protected DistributionService distributionService1;
	
	@Autowired
	@Qualifier("distributionService2")
	protected DistributionService distributionService2;
	
	@Autowired
	@Qualifier("userService1")
	protected UserService userService1;
	
	@Autowired
	@Qualifier("userService2")
	protected UserService userService2;
	
	@Autowired
	protected EndowmentService endowmentService;;
	
	@Autowired
	protected JsonService jsonService;
}
