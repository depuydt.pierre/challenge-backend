package com.challenge.wedoogift.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.DistributionLevel2;
import com.challenge.wedoogift.model.Endowment;
import com.challenge.wedoogift.model.EndowmentLevel2;
import com.challenge.wedoogift.model.UserLevel1;
import com.challenge.wedoogift.model.UserLevel2;
import com.challenge.wedoogift.model.Wallet;

@RestController
public class DistributionController extends AbstractController {

	
	@PostMapping(value = "/level1/distribute")
	@ResponseBody
	public String distributeLevel1(@RequestBody String input, @RequestParam Integer companyId,
			@RequestParam Integer userId, @RequestParam Integer distributionAmount, @RequestParam String startDate) throws Exception {
		Endowment endowment = null;
		try {
			if (input != null && companyId != null && userId != null && distributionAmount != null) {
				 endowment = jsonService.parseJsonFromString(input, Endowment.class);
				Company company = endowmentService.getCompanyById(companyId, endowment);
				UserLevel1 user = (UserLevel1) endowmentService.getUserById(userId, endowment);
				Distribution distribution = new Distribution(distributionAmount,
						LocalDate.parse(startDate));
				distributionService1.distributeGiftCard(company, user, distribution);
				if (endowment.getDistributions() == null) {
					List<Distribution> distributions = new ArrayList<>();
					endowment.setDistributions(distributions);
				}
				endowment.getDistributions().add(distribution);
			}
		} catch (Exception e) {
			throw new Exception("La distribution de niveau 1 n'a pu aboutir");
		}
		return jsonService.convertToJsonString(endowment);

	}
	
	@PostMapping(value = "/level2/distribute")
	@ResponseBody
	public String distributeLevel2(@RequestBody String input, @RequestParam Integer companyId,
			@RequestParam Integer userId, @RequestParam Integer distributionAmount, @RequestParam String startDate, @RequestParam Integer walletId) throws Exception {
		EndowmentLevel2 endowment = null;
		try {
			if (input != null && companyId != null && userId != null && distributionAmount != null) {
				endowment = (EndowmentLevel2) jsonService.parseJsonFromString(input, EndowmentLevel2.class);
				Company company = endowmentService.getCompanyById(companyId, endowment);
				UserLevel2 user = (UserLevel2) endowmentService.getUserById(userId, endowment);
				Wallet wallet = endowmentService.getWalletById(walletId, endowment);
				DistributionLevel2 distribution = new DistributionLevel2(distributionAmount,
						LocalDate.parse(startDate), wallet);
				distributionService2.distributeGiftCard(company, user, distribution);
				if (endowment.getDistributions() == null) {
					List<Distribution> distributions = new ArrayList<>();
					endowment.setDistributions(distributions);
				}
				endowment.getDistributions().add(distribution);
			}
		} catch (Exception e) {
			throw new Exception("La distribution de niveau 2 n'a pu aboutir");
		}
		return jsonService.convertToJsonString(endowment);

	}

}
