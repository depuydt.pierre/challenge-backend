package com.challenge.wedoogift.util;

import java.lang.reflect.Type;

import com.challenge.wedoogift.model.Wallet;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class WalletSerializer implements JsonSerializer<Wallet> {

	public JsonElement serialize(Wallet wallet, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(wallet.getId());
	}

}
