package com.challenge.wedoogift.util;

import java.lang.reflect.Type;

import com.challenge.wedoogift.model.Company;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CompanySerializer implements JsonSerializer<Company> {

	public JsonElement serialize(Company company, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(company.getId());
	}

}
