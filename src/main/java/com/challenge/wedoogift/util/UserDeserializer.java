package com.challenge.wedoogift.util;

import java.lang.reflect.Type;

import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.UserLevel1;
import com.challenge.wedoogift.model.UserLevel2;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class UserDeserializer implements JsonDeserializer<User> {

	@Override
	public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		Gson g = new Gson();	
			JsonObject jsonObject = json.getAsJsonObject();
			User user = new User();
			if (jsonObject.get("balance").isJsonPrimitive()) {
				 user=  g.fromJson(jsonObject, UserLevel1.class);
			} else {
				 user=  g.fromJson(jsonObject, UserLevel2.class);
			}
        return user;
	}
}
