package com.challenge.wedoogift.util;

import java.lang.reflect.Type;

import com.challenge.wedoogift.model.User;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserSerializer implements JsonSerializer<User> {

	public JsonElement serialize(User user, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(user.getId());
	}	
}
