package com.challenge.wedoogift.service;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.challenge.wedoogift.model.Balance;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.DistributionLevel2;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.UserLevel2;
import com.challenge.wedoogift.model.WalletType;

@Service("userService2")
public class UserServiceImplLevel2 implements UserService {
	@Override
	public void calculateUserBalance(User user, Distribution distribution) throws Exception {
		if (distribution instanceof DistributionLevel2 && user instanceof UserLevel2) {
			DistributionLevel2 distribution2 = (DistributionLevel2) distribution;
			UserLevel2 user2 = (UserLevel2) user;
			if (!WalletType.FOOD.equals(distribution2.getWallet().getType()) || !WalletType.GIFT.equals(distribution2.getWallet().getType())) {
				Optional<Balance> balanceOpt = null;
				Balance balance;
				if (!CollectionUtils.isEmpty(user2.getBalance())) {
					balanceOpt = user2.getBalance().stream().filter(b -> distribution2.getWallet().getId() == b.getWallet()).findFirst();				
				}
				if (balanceOpt != null && balanceOpt.isPresent()) {
					balanceOpt.get().setAmount(balanceOpt.get().getAmount() + distribution2.getAmount());
				} else {
					balance = new Balance(distribution.getAmount(), distribution2.getWallet());
					user2.getBalance().add(balance);
				}			
			} else {
				throw new Exception("Le type de Wallet n'est pas valide");
			}
		} else {
			throw new Exception("Erreur technique");
		}
	}
}
