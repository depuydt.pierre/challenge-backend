package com.challenge.wedoogift.service;

import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.User;

public interface UserService {
	void calculateUserBalance(User user, Distribution distribution) throws Exception;
}
