package com.challenge.wedoogift.service;

import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Endowment;
import com.challenge.wedoogift.model.EndowmentLevel2;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.Wallet;

public interface EndowmentService {

	Company getCompanyById(long companyId, Endowment end);

	User getUserById(long userId, Endowment end);
	
	Wallet getWalletById(long walletId, EndowmentLevel2 end);
}
