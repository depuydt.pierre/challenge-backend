package com.challenge.wedoogift.service;

import org.springframework.stereotype.Service;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.UserLevel1;



@Service("userService1")
public class UserServiceImpl implements UserService {
	@Override
	public void calculateUserBalance(User user, Distribution distribution) {
		if (user instanceof UserLevel1) {
			UserLevel1 user1 = (UserLevel1) user;
			user1.setBalance(user1.getBalance() + distribution.getAmount());			
		}
	}
	

}
