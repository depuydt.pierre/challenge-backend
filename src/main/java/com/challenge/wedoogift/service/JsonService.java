package com.challenge.wedoogift.service;


import com.challenge.wedoogift.model.Endowment;

public interface JsonService {

	Endowment parseJson(String path,Class<?> clazz) throws  Exception ;
	
	String convertToJson(Endowment end, String path) throws Exception;
	
	Endowment parseJsonFromString(String input,Class<?> clazz);
	
	String convertToJsonString(Endowment end);
}
