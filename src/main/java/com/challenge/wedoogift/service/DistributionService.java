package com.challenge.wedoogift.service;

import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.User;

public interface DistributionService {

	public void distributeGiftCard(Company company, User user, Distribution distribution) throws Exception;
}
