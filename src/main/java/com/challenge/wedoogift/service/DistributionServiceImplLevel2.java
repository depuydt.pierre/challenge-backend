package com.challenge.wedoogift.service;

import java.time.LocalDate;
import java.time.YearMonth;

import org.springframework.stereotype.Service;

import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.DistributionLevel2;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.UserLevel2;
import com.challenge.wedoogift.model.WalletType;
@Service("distributionService2")
public class DistributionServiceImplLevel2 implements DistributionService {

	private final int DURATION = 364;
	private UserService userService;
	
	public DistributionServiceImplLevel2(UserServiceImplLevel2 userService) {
		this.userService = userService;
	}
	
	public void distributeGiftCard(Company company, User user, Distribution distribution) throws Exception {
		if (user != null && company != null && distribution != null) {
			if (distribution instanceof DistributionLevel2 && user instanceof UserLevel2) {
				DistributionLevel2 distribution2 = (DistributionLevel2) distribution;
				UserLevel2 user2 = (UserLevel2) user;
				if (distribution2.getWallet() != null) {
					if (company.getBalance() < distribution2.getAmount()) {
						throw new Exception("l'entreprise " + company.getName() + " ne peut pas verser le montant de " + distribution2.getAmount());					
					}
					if (WalletType.GIFT.equals(distribution2.getWallet().getType())) {
						distribution2.setEndDate(distribution2.getStartDate().plusDays(DURATION));				
					} else if (WalletType.FOOD.equals(distribution2.getWallet().getType())) {
						distribution2.setEndDate(getMealVoucherExpirationDate(distribution2.getStartDate()));
					}
					if (distribution2.getEndDate().isBefore(LocalDate.now())) {
						if (WalletType.GIFT.equals(distribution2.getWallet().getType())) {
							throw new Exception("La carte cadeau n'est plus valide");
						} else if (WalletType.FOOD.equals(distribution2.getWallet().getType())) {
							throw new Exception("La carte ticket restaurant n'est plus valide");						
						}
					}
					company.setBalance(company.getBalance() - distribution2.getAmount());
					distribution2.setCompany(company);
					userService.calculateUserBalance(user2, distribution2);
					distribution2.setUser(user2);
					
				} else {
					throw new Exception("Le wallet n'est pas spécifié");
				}
				
			} else {
				throw new Exception("Erreur technique");
			}
		} else {
			throw new Exception("Erreur technique");
		}
	}
	
	public LocalDate getMealVoucherExpirationDate(LocalDate startDate) {
		//Permet de récupérer le dernier jour de février en prenant en compte les années bisextiles
		YearMonth yearMonth = YearMonth.of(startDate.getYear() + 1, 2);
		return LocalDate.of(startDate.getYear() + 1, 2, yearMonth.atEndOfMonth().getDayOfMonth());		
	}
}
