package com.challenge.wedoogift.service;



import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.challenge.wedoogift.model.Endowment;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.util.LocalDateAdapter;
import com.challenge.wedoogift.util.UserDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

@Service("jsonService")
public class JsonServiceImpl implements JsonService {


	private final static Gson g = new GsonBuilder().setPrettyPrinting()
			.registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
			.registerTypeAdapter(User.class, new UserDeserializer())
			.excludeFieldsWithoutExposeAnnotation()
			.create();
	
	private final static Gson g2 = new GsonBuilder().setPrettyPrinting()
			.registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
			.excludeFieldsWithoutExposeAnnotation()
			.create();
	
	@Override
	public Endowment parseJson(String path,Class<?> clazz) throws FileNotFoundException {
		JsonReader reader = new JsonReader(new FileReader(path));
		Endowment end = g.fromJson(reader, clazz);
		return end;
	}
	@Override
	public Endowment parseJsonFromString(String input,Class<?> clazz) {
		Endowment end = (Endowment) g.fromJson(input, clazz);
		return end;
	}
	@Override
	public String convertToJson(Endowment end, String path) throws IOException {
		String json = g2.toJson(end);
		Writer writer = new FileWriter(path);
		g2.toJson(end, writer);
		writer.flush();
		writer.close();
		return json;
	}
	@Override
	public String convertToJsonString(Endowment end) {
		String json = g2.toJson(end);
		g2.toJson(end);
		return json;
	}

}
