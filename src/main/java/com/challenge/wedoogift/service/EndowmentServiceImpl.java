package com.challenge.wedoogift.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Endowment;
import com.challenge.wedoogift.model.EndowmentLevel2;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.Wallet;
@Service("endowmentService")
public class EndowmentServiceImpl implements EndowmentService {

	public Company getCompanyById(long companyId, Endowment end) {
		Company company = null;
		Optional<Company> optCompany = end.getCompanies().stream().filter(c -> c.getId() == companyId).findFirst();
		if (optCompany.isPresent()) {
			company = optCompany.get();
		}
		return company;
	}
	public User getUserById(long userId, Endowment end) {
		User user = null;
		Optional<User> optUser = end.getUsers().stream().filter(u -> u.getId() == userId).findFirst();
		if (optUser.isPresent()) {
			user = optUser.get();
		}
		return user;
	}
	public Wallet getWalletById(long walletId, EndowmentLevel2 end) {
		Wallet wallet = null;
		Optional<Wallet> optWallet = end.getWallets().stream().filter(w -> w.getId() == walletId).findFirst();
		if (optWallet.isPresent()) {
			wallet = optWallet.get();
		}
		return wallet;
	}
	

}
