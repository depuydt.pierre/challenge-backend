package com.challenge.wedoogift.service;

import java.time.LocalDate;


import org.springframework.stereotype.Service;

import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.User;

@Service("distributionService1")
public class DistributionServiceImpl implements DistributionService{

	private final int DURATION = 364;
	private UserService userService;
	

	public DistributionServiceImpl(UserServiceImpl userService) {
		this.userService = userService;
	}
	public void distributeGiftCard(Company company, User user, Distribution distribution) throws Exception {
		if (user != null && company != null && distribution != null) {
			if (company.getBalance() < distribution.getAmount()) {
				throw new Exception("l'entreprise " + company.getName() + " ne peut pas verser le montant de " + distribution.getAmount());					
			} 
			distribution.setEndDate(distribution.getStartDate().plusDays(DURATION));
			if (distribution.getEndDate().isBefore(LocalDate.now())) {
				throw new Exception("La carte cadeau n'est plus valide");
			}
			company.setBalance(company.getBalance() - distribution.getAmount());
			userService.calculateUserBalance(user, distribution);
			distribution.setCompany(company);
			distribution.setUser(user);
							
		} else {
			throw new Exception("Erreur technique");
		}
	}
	


}
