package com.challenge.wedoogift;

import java.time.LocalDate;

import com.challenge.wedoogift.model.DistributionLevel2;
import com.challenge.wedoogift.model.EndowmentLevel2;
import com.challenge.wedoogift.service.DistributionService;
import com.challenge.wedoogift.service.DistributionServiceImplLevel2;
import com.challenge.wedoogift.service.JsonService;
import com.challenge.wedoogift.service.JsonServiceImpl;
import com.challenge.wedoogift.service.UserServiceImplLevel2;

public class MainLevel2 {
	private final static String inputFile = "backend/Level2/data/input.json";
	private final static String outputFile = "backend/Level2/data/output.json";
	public static void main(String[] args) throws Exception {
		UserServiceImplLevel2 userService = new UserServiceImplLevel2();
		DistributionService distributionService = new DistributionServiceImplLevel2(userService);
		JsonService jsonService = new JsonServiceImpl();
		EndowmentLevel2 end = (EndowmentLevel2) jsonService.parseJson(inputFile,EndowmentLevel2.class);
		DistributionLevel2 distribution1 = new DistributionLevel2(50, LocalDate.of(2021,9,16), end.getWallets().get(0));
		DistributionLevel2 distribution2 = new DistributionLevel2(100, LocalDate.of(2021,8,1), end.getWallets().get(0));
		DistributionLevel2 distribution3 = new DistributionLevel2(1000, LocalDate.of(2021,5,1), end.getWallets().get(0));
		DistributionLevel2 distribution4 = new DistributionLevel2(250, LocalDate.of(2021,5,1), end.getWallets().get(1));
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(0), distribution1);
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(1), distribution2);
		distributionService.distributeGiftCard(end.getCompanies().get(1), end.getUsers().get(2), distribution3);
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(0), distribution4);
		end.getDistributions().add(distribution1);
		end.getDistributions().add(distribution2);
		end.getDistributions().add(distribution3);
		end.getDistributions().add(distribution4);
		jsonService.convertToJson(end, outputFile);
	}

}
