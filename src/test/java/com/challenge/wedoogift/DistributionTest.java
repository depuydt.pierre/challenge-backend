package com.challenge.wedoogift;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.challenge.wedoogift.model.Balance;
import com.challenge.wedoogift.model.Company;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.DistributionLevel2;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.UserLevel1;
import com.challenge.wedoogift.model.UserLevel2;
import com.challenge.wedoogift.model.Wallet;
import com.challenge.wedoogift.model.WalletType;
import com.challenge.wedoogift.service.DistributionService;
import com.challenge.wedoogift.service.DistributionServiceImpl;
import com.challenge.wedoogift.service.DistributionServiceImplLevel2;
import com.challenge.wedoogift.service.UserServiceImpl;
import com.challenge.wedoogift.service.UserServiceImplLevel2;

public class DistributionTest {
	
	private UserServiceImpl userService = new UserServiceImpl();
	private DistributionService distributionService = new DistributionServiceImpl(userService);
	private UserServiceImplLevel2 userService2 = new UserServiceImplLevel2();
	private DistributionService distributionService2 = new DistributionServiceImplLevel2(userService2);
	private Company company = null;
	private User user = null;
	private Distribution distribution = null;
	private Wallet wallet = null;
	
	
	@Before
	public void initObject() {
		user = new UserLevel1(100);
		company = new Company("Wedoogift" ,500);
		distribution = new Distribution(150, LocalDate.now());
		wallet = new Wallet(1, "Wedoogift", WalletType.FOOD);
	}
	
	@After
	public void cleanObjects() {
		company = null;
		user = null;
		distribution = null;
	}


	@Test
	public void distributeGiftCard_nominal() throws Exception {
		
		distributionService.distributeGiftCard(company, user, distribution);
		
		assertThat(distribution.getAmount()).isEqualTo(150);
		assertThat(Period.between(distribution.getStartDate(), distribution.getEndDate()).getMonths()).isEqualTo(11);
		assertThat(Period.between(distribution.getStartDate(), distribution.getEndDate()).getDays()).isEqualTo(29);
		assertThat(((UserLevel1) user).getBalance()).isEqualTo(250);
		assertThat(company.getBalance()).isEqualTo(350);
	}
	
	@Test
	public void distributeFoodCard_nominal() throws Exception {
		DistributionLevel2 distribution2 = new DistributionLevel2(distribution, wallet);
		List<Balance> balances = new ArrayList<>();
		Balance balance = new Balance(100, wallet);
		balances.add(balance);	
		UserLevel2 user2 = new UserLevel2(user, balances);		
		distributionService2.distributeGiftCard(company, user2, distribution2);
		
		assertThat(distribution2.getAmount()).isEqualTo(150);
		assertThat(distribution2.getEndDate()).isEqualTo(LocalDate.of(2022, 2, 28));
		assertThat(user2.getBalance().get(0).getAmount()).isEqualTo(250);
		assertThat(company.getBalance()).isEqualTo(350);
	}
	
	@Test
	public void distributeGiftCard_error_companyBalance() {
		company = new Company("Wedoogift", 0);
		assertThatExceptionOfType(Exception.class).isThrownBy(new ThrowingCallable() {
			public void call() throws Throwable {
				distributionService.distributeGiftCard(company, user, distribution);
			}
		}).withMessageContaining("l'entreprise " + company.getName() + " ne peut pas verser le montant de " + distribution.getAmount());
		
		assertThat(distribution.getUser()).isNull();
		assertThat(distribution.getCompany()).isNull();
		assertThat(distribution.getEndDate()).isNull();
	}
	
	@Test
	public void distributeGiftCard_error_expiredCard() {
		distribution.setStartDate(LocalDate.of(2020, 8, 1));
		assertThatExceptionOfType(Exception.class).isThrownBy(new ThrowingCallable() {
			public void call() throws Throwable {
				distributionService.distributeGiftCard(company, user, distribution);
			}
		}).withMessageContaining("La carte cadeau n'est plus valide");
		assertThat(distribution.getUser()).isNull();
		assertThat(distribution.getCompany()).isNull();
		assertThat(distribution.getEndDate()).isEqualTo(LocalDate.of(2021, 7, 31));
	}
	
	@Test
	public void distributeGiftCard_nullObject() {
		user = null;
		company = null;
		assertThatExceptionOfType(Exception.class).isThrownBy(new ThrowingCallable() {
			public void call() throws Throwable {
				distributionService.distributeGiftCard(company, user, distribution);
			}
		}).withMessageContaining("Erreur technique");
	}
}
