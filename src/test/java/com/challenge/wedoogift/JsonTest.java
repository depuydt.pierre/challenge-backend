package com.challenge.wedoogift;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

import org.junit.Test;

import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.Endowment;
import com.challenge.wedoogift.service.DistributionService;
import com.challenge.wedoogift.service.DistributionServiceImpl;
import com.challenge.wedoogift.service.JsonService;
import com.challenge.wedoogift.service.JsonServiceImpl;
import com.challenge.wedoogift.service.UserServiceImpl;

public class JsonTest {
	private final static String inputFile = "backend/Level1/data/input.json";
	private final static String outputFile = "backend/Level1/data/output.json";
	private JsonService jsonService = new JsonServiceImpl();

	@Test
	public void fromJson() throws Exception {
		Endowment end = jsonService.parseJson(inputFile,Endowment.class);
		
		assertThat(end).isNotNull();
		assertThat(end.getCompanies().size()).isEqualTo(2);
		assertThat(end.getUsers().size()).isEqualTo(3);
		assertThat(end.getDistributions()).isNullOrEmpty();
	}
	
	@Test
	public void toJson() throws Exception {
		UserServiceImpl userService = new UserServiceImpl();
		DistributionService distributionService = new DistributionServiceImpl(userService);
		Endowment end = jsonService.parseJson(inputFile,Endowment.class);
		Distribution distribution1 = new Distribution(50, LocalDate.of(2021,9,16));
		Distribution distribution2 = new Distribution(100, LocalDate.of(2021,8,1));
		Distribution distribution3 = new Distribution(1000, LocalDate.of(2021,5,1));
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(0), distribution1);
		distributionService.distributeGiftCard(end.getCompanies().get(0), end.getUsers().get(1), distribution2);
		distributionService.distributeGiftCard(end.getCompanies().get(1), end.getUsers().get(2), distribution3);
		end.getDistributions().add(distribution1);
		end.getDistributions().add(distribution2);
		end.getDistributions().add(distribution3);
		String jsonToTest = jsonService.convertToJson(end, outputFile);
		String output = new String (Files.readAllBytes( Paths.get(outputFile)));
		assertThat(jsonToTest).isEqualTo(output);
	}
}
