package com.challenge.wedoogift;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.challenge.wedoogift.model.Balance;
import com.challenge.wedoogift.model.Distribution;
import com.challenge.wedoogift.model.DistributionLevel2;
import com.challenge.wedoogift.model.User;
import com.challenge.wedoogift.model.UserLevel1;
import com.challenge.wedoogift.model.UserLevel2;
import com.challenge.wedoogift.model.Wallet;
import com.challenge.wedoogift.model.WalletType;
import com.challenge.wedoogift.service.UserService;
import com.challenge.wedoogift.service.UserServiceImpl;
import com.challenge.wedoogift.service.UserServiceImplLevel2;

public class UserTest {

	@Test
	public void calculateUserBalance() throws Exception {
		UserService userService = new UserServiceImpl();
		User user = new UserLevel1(100);
		Distribution distribution = new Distribution(100, LocalDate.now());
		userService.calculateUserBalance(user, distribution);
		
		assertThat(((UserLevel1) user).getBalance()).isEqualTo(200);
	}
	
	@Test
	public void calculateUserBalance2() throws Exception {
		UserService userService = new UserServiceImplLevel2();
		List<Balance> balances = new ArrayList<>();
		Wallet wallet = new Wallet(1, "Wedoogift", WalletType.GIFT);
		Balance balance = new Balance(150, wallet);
		balances.add(balance);
		User user = new UserLevel2(balances);
		Distribution distribution = new DistributionLevel2(100, LocalDate.now(), wallet);
		userService.calculateUserBalance(user, distribution);
		
		assertThat(((UserLevel2) user).getBalance().get(0).getAmount()).isEqualTo(250);
	}
}
